/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import tools from '../../services/apis.js'
class Register extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "",
      email: "",
      nom: "",
      prenom: "",
      tel: "",
      rc: "",
      mf: '',
      adresse: "",
      password: "",
      comm: ""
    }
  }
  async signup() {
    if (this.state.password == "" || this.state.email == "" || this.state.nom == "" || this.state.prenom == "") {
      alert("Veuillez remplir les champs requis")
    }
    else {
      await fetch(tools.createemp(1), {
        method: 'POST',
        body: JSON.stringify({
          "username": this.state.nom + this.state.prenom,
          "email": this.state.email,
          "password": this.state.password,
          "nom": this.state.nom,
          "prenom": this.state.prenom,
          "comm": this.state.comm
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(results => {
          return results.text()
        }).then(data => {
          console.log(data)
          if (data == "Account created") {
            alert("Compte a éte crée avec succes")
            this.props.history.replace(`/admin/ListesRevendeurs`)
          }
          else
            if (JSON.parse(data).errors[0].defaultMessage == "la taille doit être comprise entre 3 et 15") {
              alert("la taille de nom d'utilisateur doit être comprise entre 3 et 15")
            }
            else if (JSON.parse(data).errors[0].defaultMessage == "la taille doit être comprise entre 6 et 20")
              alert("Mot de passe trop court")
            else
              alert("Voulez vous verifier votre email")
        })
    }

  }
  render() {
    return (
      <>
        <Col style={{ marginTop: -60 }} lg="6" md="8">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div style={{ marginTop: -10 }} className="text-center text-muted mb-4">
              </div>
              <Form role="form">
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ email: text.target.value })} placeholder="Email" type="email" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ password: text.target.value })} placeholder="Mot de passe" type="password" autoComplete="new-password" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-single-02" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ nom: text.target.value })} placeholder="Nom" type="text" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-hat-3" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ prenom: text.target.value })} placeholder="Prenom" type="text" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-align-center" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ comm: text.target.value })} placeholder="Commission" type="text" />
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button style={{ backgroundColor: "#fe0c00", width: 348 }} onClick={() => this.signup()} className="my-4" color="danger" type="button">
                    Créer un compte
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Register;
