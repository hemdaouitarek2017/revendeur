import React, { Component } from 'react';
import {
    Badge,
    Card,
    CardHeader,
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,
    Table,
    Container,
    Row,
  } from "reactstrap";
import { Radio, Form } from 'semantic-ui-react'
import tools from "../../services/apis.js"
import ReactDatetime from "react-datetime";
import moment from "moment";
import Header from "components/Headers/Header.js";

class ListEmploye extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 0,
            Listemp: []
        };
    }
    handleClick(e, index) {

        e.preventDefault();

        this.setState({
            currentPage: index
        });

    }
    componentWillMount() {
        this.getEmployes()
        this.pageSize = 10;
    }
    async getEmployes() {
        await fetch(tools.getempbyrevendeur(1), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results.json()
            }).then(data => {
                console.log(data)
                this.pagesCount = Math.ceil(data.length / this.pageSize);
                this.setState({ Listemp: data })
            })
    }
    async garantie(id) {
        await fetch(tools.garant(id), {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results
            }).then(data => {
                alert("Le garantie a éte supprime")
                console.log(data)
                this.setState({
                    ListGaranties: this.state.ListGaranties.filter(item => item.id !== id),
                });
            })
    }
    showRes(item) {
        return (
            <tr>
                <th scope="row">
                    <a className="mb-0 text-sm">{item.id}</a>
                </th>
                <th scope="row">
                    <a className="mb-0 text-sm">{item.nom}</a>
                </th>
                <th scope="row">
                    <a className="mb-0 text-sm">{item.prenom}</a>
                </th>
                <th scope="row">
                    <a className="mb-0 text-sm">{item.comm}</a>
                </th>
            </tr>
        )
    }
    render() {
        const { currentPage } = this.state;
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="border-0">
                                    <h3 className="mb-0">Liste des employes</h3>
                                </CardHeader>
                                <Table className="align-items-center table-flush" responsive>
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Id</th>
                                            <th scope="col">Nom</th>
                                            <th scope="col">Prenom</th>    
                                            <th scope="col">Commission</th>                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.Listemp.slice(
                                                currentPage * this.pageSize,
                                                (currentPage + 1) * this.pageSize
                                            ).map((t) => this.showRes(t))
                                        }
                                    </tbody>
                                </Table>
                                <div className="pagination-wrapper">
                                    <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                                        listClassName="justify-content-end mb-0">
                                        <PaginationItem disabled={currentPage <= 0}>
                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage - 1)}
                                                previous
                                                href="#"
                                            />
                                        </PaginationItem>
                                        {[...Array(this.pagesCount)].map((page, i) =>
                                            <PaginationItem active={i === currentPage} key={i}>
                                                <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                                                    {i + 1}
                                                </PaginationLink>
                                            </PaginationItem>
                                        )}
                                        <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage + 1)}
                                                next
                                                href="#"
                                            />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

export default ListEmploye;
